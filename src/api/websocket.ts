import { io } from 'socket.io-client';
import { wsBase } from './config';

const socket = io(wsBase);

socket.on('connection', (s) => {
  console.log(`Testing ${s}`);
});
