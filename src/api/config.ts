export const apiBase = process.env.VUE_APP_API_REST_URL || 'http://localhost:4000/v1';
export const wsBase = process.env.VUE_APP_API_WS_URL || 'ws://localhost:4000/v1/ws';
