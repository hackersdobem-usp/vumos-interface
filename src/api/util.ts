import axios from 'axios';
import { toCamel, toSnake } from 'snake-camel';
import { Ref, ref } from 'vue';
import { apiBase } from './config';

/* eslint-disable import/prefer-default-export */
export const getURL = (path: string): string => `${apiBase}${path}`;

export const getPoolingModule = <T extends unknown>(url: string, every = 15): {
  // Pagination Information
  paginate: (page: number, rows: number) => void,
  orderby: (dict: { [key: string]: number }) => void,
  page: Ref<number>,
  rows: Ref<number>,
  total: Ref<number>,
  loading: Ref<boolean>,
  objects: Ref<T[]>,
  countdown: Ref<number>,
  interval: Ref<number>
} => {
  // Object list (polled)
  const objects: Ref<T[]> = ref([]);

  const loading = ref(true);
  const countdown = ref(0);

  // Pagination
  const page = ref(0);
  const rows = ref(0);
  const total = ref(0);

  const paginate = (p: number, r: number) => {
    loading.value = true;
    page.value = p;
    rows.value = r;
    countdown.value = 0;
  };

  const order: Ref<{ [key: string]: number }> = ref({});

  const orderby = (dict: { [key: string]: number }) => {
    order.value = toSnake(dict) as { [key: string]: number };
    console.log(order);
    countdown.value = 0;
  };

  // Pooling
  const interval = ref(setInterval(async () => {
    countdown.value -= 1;

    const sortstr = Object.keys(order.value).map((key) => `${key}:${order.value[key] > 0 ? 'asc' : 'desc'}`).join(',');

    if (countdown.value <= 0) {
      countdown.value = every;
      const { data } = await axios.get(getURL(url), {
        params: {
          page: page.value,
          rows: rows.value,
          sort: sortstr,
        },
      });

      total.value = data.data.total;
      objects.value = (data.data.list as Record<string, unknown>[])
        .map((mod: Record<string, unknown>) => toCamel(mod) as T);
      loading.value = false;
    }
  }, 1000));

  return {
    paginate,
    orderby,
    page,
    rows,
    total,
    loading,
    objects,
    countdown,
    interval,
  };
};

export const getDetails = <T extends unknown>(url: string): {
  loading: Ref<boolean>,
  object: Ref<T | undefined>
} => {
  // Object
  const object: Ref<T | undefined> = ref(undefined);

  const loading = ref(true);

  axios.get(getURL(url)).then((val) => {
    object.value = toCamel(val.data.data as Record<string, unknown>) as T;

    loading.value = false;
  });

  return {
    loading, object,
  };
};
