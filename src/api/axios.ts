/* eslint-disable no-param-reassign */
import axios, { AxiosResponse } from 'axios';
import router from '@/router';
import store from '@/store';

// Attach Authorization header
axios.interceptors.request.use((config) => {
  const { token } = store.state.auth;

  if (token) config.headers.Authorization = `Bearer ${token}`;

  return config;
});

// Handle request errors
axios.interceptors.response.use((response) => response, async (error) => {
  const { response } = error as { response: AxiosResponse };

  // On authorization error
  if (response.status === 401) {
    await store.dispatch('auth/logout');
    router.replace({ name: 'home' });
  }
  return Promise.reject(error);
});
