export interface Module {
  id: string
  uid: string

  name: string
  nickname: string

  description: string

  statusMessage: string
  statusCode: string
}
