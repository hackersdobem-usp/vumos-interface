export interface Issue {
  id: string

  name: string
  description: string
  cve: string
  cvss: string

  extra?: string
  notes?: string
}
