export interface Service {
  id: string

  port: number
  name: string
  protocol: string
  version: string

  extra?: string
  notes?: string
}
