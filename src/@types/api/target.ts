import { Service } from './service';

export interface Target {
  id: string
  ipAddress: string

  domains: string[]

  services?: Service[]

  extra?: string
  notes?: string
}
