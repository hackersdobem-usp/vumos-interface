import {
  createRouter, createWebHistory, RouteLocationRaw, RouteRecordRaw,
} from 'vue-router';
import store from '@/store';
import dashboard from './dashboard';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    redirect: (): RouteLocationRaw => {
      if (store.state.auth.token) {
        return { name: 'dashboard' };
      }
      return { name: 'login' };
    },
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login.vue'),
    beforeEnter: (to, from, next) => {
      next(() => {
        console.log(store.state);
        if (store.state.auth.token) next({ name: 'home' });
        else next();
      });
    },
  },
  dashboard,
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
