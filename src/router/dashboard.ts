import { RouteRecordRaw } from 'vue-router';
import store from '@/store';

const dashboard: RouteRecordRaw = {
  path: '/dashboard',
  name: 'dashboard',
  component: () => import('../views/Dashboard.vue'),
  beforeEnter: (to, from, next) => {
    next(() => {
      if (store.state.auth.token) next({ name: 'home' });
      else next();
    });
  },
  redirect: { name: 'dashboard-home' },
  children: [{
    path: 'home',
    name: 'dashboard-home',
    component: () => import('../views/dashboard/Home.vue'),
  }, {
    path: 'data',
    name: 'dashboard-data',
    component: () => import('../views/dashboard/Data.vue'),
    redirect: { name: 'data-targets' },
    children: [
      {
        path: 'target/:id',
        name: 'target-details',
        component: () => import('../views/dashboard/data/details/Target.vue'),
      },
      {
        path: 'targets',
        name: 'data-targets',
        component: () => import('../views/dashboard/data/Targets.vue'),
      },
      {
        path: 'services',
        name: 'data-services',
        component: () => import('../views/dashboard/data/Services.vue'),
      },
      {
        path: 'issues',
        name: 'data-issues',
        component: () => import('../views/dashboard/data/Issues.vue'),
      },
    ],
  }],
};

export default dashboard;
