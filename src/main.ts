import { createApp } from 'vue';
import PrimeVue from 'primevue/config';
import JsonViewer from 'vue3-json-viewer';

import App from '@/App.vue';
import '@/registerServiceWorker';
import router from '@/router';
import store from '@/store';

import '@/api/axios';
import '@/api/websocket';

const app = createApp(App)
  .use(store)
  .use(router);
/*
const villusClient = useClient({
  url: process.env.VUE_APP_API_URL || 'http://10.10.11.218:3000/',
});

villusClient.install(app);
*/
app.use(PrimeVue);
app.use(JsonViewer);

app.mount('#app');
