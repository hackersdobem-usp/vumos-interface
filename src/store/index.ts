import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';
import { auth, AuthState } from './auth';

const persistence = new VuexPersistence<{ auth: AuthState }>({
  storage: window.localStorage,
});

const store = new Vuex.Store<{ auth: AuthState }>({
  modules: {
    auth,
  },
  plugins: [persistence.plugin],
});

export default store;
