import axios from 'axios';
import { Module } from 'vuex';
import { getURL } from '@/api/util';

// State
export interface AuthState {
  token: string | null
}

export const auth: Module<AuthState, { auth: AuthState }> = {
  namespaced: true,
  state: (): AuthState => ({
    token: null,
  }),
  mutations: {
    setToken: (state: AuthState, token: string | null): void => {
      state.token = token;
    },

  },
  actions: {
    login: async ({ commit }, { email, password }: { email: string, password: string }) => {
      // Hello
      try {
        const loginResponse = await axios.post(getURL('/auth/login'), {
          email,
          password,
        });

        commit('setToken', loginResponse.data.data.token);
      } catch (e) {
        console.error(e);
      }
    },
    logout: ({ commit }) => {
      commit('setToken', null);
    },
  },
};
