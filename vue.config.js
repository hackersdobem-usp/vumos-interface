module.exports = {
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.(graphql|gql)$/,
          use: 'raw-loader',
        },
      ],
    },
  },
};
